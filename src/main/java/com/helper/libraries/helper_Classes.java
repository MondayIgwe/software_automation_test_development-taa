package com.helper.libraries;

import java.util.Scanner;

public class helper_Classes {


    //****** GROSS JAVA PROJECTS*********
    private double rate;
    private int hour;
    private double grossPay;

    //Initialize known values for Salary Project
    public static final int QUOTA = 10;
     int salary = 1000;
     int bonus = 250;

    Scanner scanner = new Scanner(System.in);


    //Get the number of hours
    public int getNumberOfHours() {
        System.out.println("Please Enter hours");
        hour = scanner.nextInt();
        return hour;
    }

    //Get the hourly pay rate
    public void getHourlyPayRate(){
        System.out.println("Enter the employee pay rate");
        rate = scanner.nextDouble();
    }

    //Multiple hours and pay rate
    public void multiplyHoursandPay(){
        grossPay = hour * rate;
    }

    //Display result
    public void printPay(){
       System.out.println("The employee's gross pay is R"+grossPay);
       scanner.close();
    }

    //**********SALARY ALGORITHMS*******************//
    public void getSalaryValues(){
        System.out.println("How many sales did the employee make this week?");
        int sales = scanner.nextInt();
        scanner.close();

        if (sales >= QUOTA){
            salary = salary + bonus;
            System.out.println("Congrat! The employee's Pay plus Bonus is R"+salary);
        }else {
            System.out.println("The employee's pay is R"+salary);
        }

    }
    
    public void getQuota(){
        System.out.println("How many sales did the employee make this week?");
        int sales = scanner.nextInt();
        scanner.close();

        if (sales>= QUOTA){
            System.out.println("Congrat! You met your quota");
        }else {
            int saleShort = QUOTA - sales;
            System.out.println("you do not make your quota. " +saleShort+ " sales short");
        }

    }
}
