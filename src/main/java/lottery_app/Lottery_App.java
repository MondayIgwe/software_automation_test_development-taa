package lottery_app;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Lottery_App {

    public static void main(String[] args) {
        Lottery_App lottery = new Lottery_App();
        //lottery.getLotteryNumber();
        lottery.getLottery();
    }
    Scanner scanner = new Scanner(System.in);

    public void getLotteryNumber(){
        System.out.println("Enter a lotto number between 1 and 50");
        int min = 1;
        int max = 50;

        int num = scanner.nextInt();
        if (num>=min && num<=max) {

            Random random = new Random();
            int i = random.nextInt();

            System.out.println((int)Math.abs(i));
        }
    }

    public void getLottery(){
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int min = 1, max = 50, slots = 6;
        int[] lotteryArray =  new int[slots];
        boolean isRepeated;

        System.out.println("Enter a number");
        int n = scanner.nextInt();

        for (int indexDrawn = 0; true; indexDrawn++){
            do{
                isRepeated = false;
                n = random.nextInt(max + 1 - min) + min;
                for (int k = 0; k<=indexDrawn;k++){
                    if (lotteryArray[k] == n){
                        isRepeated = true;
                        break;
                    }
                }
            }while (isRepeated);
                lotteryArray[indexDrawn] = n;

            Arrays.sort(lotteryArray);
            System.out.println("The result is ");
            for (int i=1;i<slots;i++){
                System.out.print(lotteryArray[i]+ " ");
            }
            break;
        }
    }
}
