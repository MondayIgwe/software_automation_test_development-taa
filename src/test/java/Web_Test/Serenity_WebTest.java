package Web_Test;


import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Step;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

@RunWith(SerenityRunner.class)
public class Serenity_WebTest {

    @Managed
    WebDriver myDriver;

    @Test
    @Step("open URL")
    public void myGoogle(){
        myDriver.get("https://flymango.com/");
        myDriver.manage().timeouts().pageLoadTimeout(1, TimeUnit.MINUTES);
        myDriver.manage().window().maximize();
        myDriver.close();
    }
}
