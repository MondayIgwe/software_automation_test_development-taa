package rest_Assured_Project;

import com.tngtech.java.junit.dataprovider.*;
import org.junit.runner.RunWith;
import org.testng.annotations.DataProvider;

@RunWith(DataProviderRunner.class)
public class Data_DrivenTest implements Country_Codes {

    @DataProvider(name="dataDriven")
    public static Object[][] data_Collection()throws Exception{
        return new Object[][]{
                {COUNTRY_CODE_USA,ZIPCODE_USA,"Beverly Hills"},
                {COUNTRY_CODE_USA,ZIPCODE_USA,"California"}
                //{COUNTRY_CODE_USA,ZIPCODE_USA,"Beverly Hills"}
        };
    }
}
