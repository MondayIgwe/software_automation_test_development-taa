package rest_Assured_Project;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static org.testng.Assert.assertEquals;

public class TestSetUp{

    public static final String SETUP = "src/test/java/setup.properties";
    public static String URL;

//<editor-fold des="Initialization the HOST url from the Properties file">
    @BeforeTest
    public static void init(){
        try (InputStream input = new FileInputStream(SETUP))
        {
            if (input != null){
               Properties prop = new Properties();

               //Load a properties file
               prop.load(input);

               //Test, Verify and get the properties file, Then pass it as a static method to the BASEURL
            URL = (prop.getProperty("url"));
            }else{
                throw new FileNotFoundException("properties file "+ SETUP);
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());

        }finally {
        }

    }
//</editor-fold>
//<editor-fold des="Load XML File">
    public void loadXML_File() throws IOException {
        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String iconConfigPath = rootPath + "src/test/java/icons.xml";
        Properties iconProps = new Properties();
        iconProps.loadFromXML(new FileInputStream(rootPath + "icons.xml"));

        String check = "icon1.jpg";
        //Assertions
        assertEquals(check,iconProps.getProperty("fileIcon"));
        System.out.println(check);
    }
//</editor-fold>
}
