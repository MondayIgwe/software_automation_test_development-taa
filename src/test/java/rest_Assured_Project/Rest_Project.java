package rest_Assured_Project;

import com.tngtech.java.junit.dataprovider.UseDataProvider;

import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
public class Rest_Project extends TestSetUp implements Country_Codes{

//<editor-fold des="USA API">
@Test
public void get_USA() throws Exception{
    String placeName = given().spec(Create_Specification.setRequestSpec())
            .when()
            .get(COUNTRY_CODE_USA+"/"+ZIPCODE_USA)
            .then()
            .spec(Create_Specification.getResponse()).root("place")
            .extract().path("places[0].'place name'").toString();
             Assert.assertEquals(placeName,"Beverly Hills");
            System.out.println("place name value is "+placeName);
}

//</editor-fold>
        @Test
        public void get_CH() throws Exception{
            //baseURI = URL;
          given().spec(Create_Specification.setRequestSpec())
                    .when()
                    .get(COUNTRY_CODE_CH+"/"+ZIPCODE_CH)
                    .then()
                    .spec(Create_Specification.getResponse())
                    .body("places[2].'state'",equalTo("Canton de Vaud"))
                    .assertThat().log().all();

}

    @Test
    public void get_CH2() throws Exception{
        //baseURI = URL;
        //This test will return the last value on a node key
         given().spec(Create_Specification.setRequestSpec())
                .when()
                .get(COUNTRY_CODE_CH+"/"+ZIPCODE_CH)
                .then()
                .spec(Create_Specification.getResponse())
                .body("places[-1].'latitude'",equalTo("46.5417"));

    }
    @Test
    public void get_CH3() throws Exception{
        //baseURI = URL;
        given().spec(Create_Specification.setRequestSpec())
                .when()
                .get(COUNTRY_CODE_CH+"/"+ZIPCODE_CH)
                .then()
                .spec(Create_Specification.getResponse())
                .body("places.findAll{it.state!='Canton de Vaud'}", empty());
    }
}
