package rest_Assured_Project;

public interface Country_Codes {

   String COUNTRY_CODE_USA = "us";
   int ZIPCODE_USA = 90210;
   String COUNTRY_CODE_BG = "bg";
   String ZIPCODE_BG = "1000";
   String COUNTRY_CODE_CH = "ch";
   int ZIPCODE_CH = 1000;
}
