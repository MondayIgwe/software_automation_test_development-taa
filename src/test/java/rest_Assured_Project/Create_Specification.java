package rest_Assured_Project;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.lessThan;

public class Create_Specification implements Country_Codes{

 private static RequestSpecification requestSpec;
 private static ResponseSpecification responseSpec;
 private static RequestSpecBuilder requestSpecBuilder;
 private  static  ResponseSpecBuilder responseSpecBuilder;

 public static RequestSpecification setRequestSpec(){
    return requestSpec = new RequestSpecBuilder().
            setBaseUri("http://api.zippopotam.us/").
            setContentType(ContentType.JSON).
            build();
 }

 public static ResponseSpecification getResponse(){
     return responseSpec = new ResponseSpecBuilder()
           .expectStatusCode(200).expectContentType(ContentType.JSON)
           .build();
 }

    public static RequestSpecification genericRequestSpec() {
        try {
            requestSpecBuilder = new RequestSpecBuilder();
            requestSpecBuilder.setContentType(ContentType.JSON);
            requestSpecBuilder.addHeader("Content-Type","application/json");
            requestSpecBuilder.addHeader("Access-Control-Allow-Origin","*");
            requestSpecBuilder.addCookie("__cfduid","d83a1cf97e49d083e0d72e1cce03db8c21574413022");
            requestSpec = requestSpecBuilder.build();


        }catch (Exception ex){
            System.out.print(ex.getMessage());
        }
        return requestSpec;
    }

    public static ResponseSpecification genericResponse() {
        try
        {
            responseSpecBuilder = new ResponseSpecBuilder();
            responseSpecBuilder.expectHeader("Content-Type", "application/json");
            responseSpecBuilder.expectResponseTime(lessThan(60L), TimeUnit.SECONDS);
            responseSpec = responseSpecBuilder.build();

        }catch (Exception ex){
            System.out.print(ex.getMessage());
        }
        return responseSpec;
    }
}
