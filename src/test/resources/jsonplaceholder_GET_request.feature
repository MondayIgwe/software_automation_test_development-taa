Feature: Testing JASONPLACEHODER API
# classpath:relative path
  Background:
    * url  anotherUrlBase
    * def jsonFile = callonce read('classpath:JASON_Files/jasonplaceholder.json')

    @parallel=false
    #@RegressionTest
    Scenario: Testing GET request API
      Given path 'users/1'
      When method GET
      Then status 200
      And def result = response
      And match $.username == 'Bret'

      * print result
