Feature: XML Api Testing

  Background:

    Scenario: API TEST
      * def cat = <cat><name>Billie</name></cat>
      * set cat /cat/name = 'Jean'
      * match cat / == <cat><name>Jean</name></cat>
      * print cat

     * def foo = {hello:'world', band:'jazz'}
     * def songs = {music: 'raggea', band: 'pop'}
     * match foo != songs
     * match foo.hello == 'world'

      * def cat = {name: ''}
      * set cat
      |path|value|
      |name|'Bob'|
      |age | 5   |

      * match cat == {name: 'Bob', age: 5}

      * set foo.bar
      |path|value|
      |one |1    |
      |two[]|2   |
      |two[]|3   |

      * match foo == {bar: {one 1, two: [2,3]}

      Scenario: REMOVE TEST
      * def json = {woo: 'hi', hey:'ho', zee: [1,2,3]}
      * remove json.hey
      * match json == {woo:'hi',zee:[1,2,3]}
      * remove json $.zee[1]
      * match json == {woo:'hi', zee:[1,3]}

