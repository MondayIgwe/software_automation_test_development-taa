Feature:  Practice Demo
  Background:
    * def myJson = {foo: 'bar', baz: [1,2,3]}
    * def cats = [{myName: 'Friday'},{surname: 'Sunday'}]
    * def cat = {name:'Billie', scores:[2, 5]}
   # * def someJson = read('classpath:Student_payload.json')

    
    Scenario: Test 1
      Given def name = 'Monday'
      And def num = 5;
      Then assert name + num == 'Monday5'
      Then print 'My name is:',name

      #Test 2
      * print 'The value of myJson is:\n',myJson
      * print 'This are the categories:', cat
      * match cat == {name: 'Billie', scores:[2, 5]}
      * assert cat.scores[0] == 2
      * assert cat.scores[1] == 5

      * match cats[1] == {surname:'Sunday'}
      * def first = cats[0]
      * match first == {myName: 'Friday'}
      * print first
      
      * def countries = {code:'NG', currency:'Naira'}
      * set countries.currency = 'US'
      * match countries == {currency: 'US'}

      # XML Test
      Given def types =
      """
        <food>
          <name1>Rice</name1>
          <name2>Beans</name>
        </food>
      """
      Then match types/food/name1[0] == Rice

      #Test EMBEDDED EXPRESSION
      Given def user =
      """
        {name:'john', age:33}
        {names:'john', ages:33}
      """
      And def lang = 'en'
      When def session = {name: '#(user.name)', local:'#(lang)', sessionUser: '#(user)'}

      #MULTILINE EXPRESSION
# this is more readable:
* def cat =
  """
  <cat>
      <name>Billie</name>
      <scores>
          <score>2</score>
          <score>5</score>
      </scores>
  </cat>
  """

    # example of a request payload in-line
#    Given request
#      """
#      <?xml version='1.0' encoding='UTF-8'?>
#      <S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
#      <S:Body>
#      <ns2:QueryUsageBalance xmlns:ns2="http://www.mycompany.com/usage/V1">
#          <ns2:UsageBalance>
#              <ns2:LicenseId>12341234</ns2:LicenseId>
#          </ns2:UsageBalance>
#      </ns2:QueryUsageBalance>
#      </S:Body>
#      </S:Envelope>
#      """

    # example of a payload assertion in-line
    #Then match response ==
      """
      { id: { domain: "DOM", type: "entityId", value: "#ignore" },
        created: { on: "#ignore" },
        lastUpdated: { on: "#ignore" },
        entityState: "ACTIVE"
      }
      """

      #TABLE

