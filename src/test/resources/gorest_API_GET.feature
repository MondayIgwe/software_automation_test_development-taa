Feature: GoRest API
  Background:
    * url baseUrlBase
    * def myPath = 'users'
    * param access-token = 'NAL2AZVtMe6wUsNlflJstJItjwWavap0OL7J'
    * param _format = 'json'
    * params {searchBy: 'client', active: true, someList: [1, 2, 3] }
    * cookie PHPSESSID = 'tt0lf335mjm8piitbout5l70j4'
    * configure headers = {'Content-Type':'text/html'}
    * cookies {_csrf:'fb5aa145e882212a7626dfeb7af72f92b8ddb94e440c83388b94082ac1d1fd58a%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22_csrf%22%3Bi%3A1%3Bs%3A32%3A%224Em1gltuXrBCUNWMGe4CrM0fCjmKj1L4%22%3B%7D', PHPSESSID: 'tt0lf335mjm8piitbout5l70j4'}
    #* cookies = null
    * def gorestPayload = callonce read('classpath:JASON_Files/gorest.json')

    @smokeTest
    @RegressionTest
  Scenario: GoRest API
    Given path 'public-api/'+ myPath
    #And form field username = 'cc311072'
    #And form field password = 'N@dgfnfnf9220!!@'
    And header Accept = 'application.xml'
    # And retry until response.id > 1
    And retry until responseStatus == 200
    When method get
    Then status 200
    * def myResponse = response
    * match myResponse._meta.code contains 200
    * print myResponse


  Scenario:
    Given request callonce read('classpath:JASON_Files/gorest.json')
    And  method put
    Then status 200