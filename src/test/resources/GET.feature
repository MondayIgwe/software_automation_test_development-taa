Feature: Practice post Api
  Background: 
    * url 'http://localhost:3000'
    * configure proxy = 'http://172.17.12.2:80'
    * configure proxy = {uri: 'http://172.17.12.2:80, username: 'myUsername', password: 'secretsKey'}
    * header Accept = 'application/json'
    * def resource = '/profile'
    * def getJson = read('classpath:JASON_Files/request.json')
    * configure cookies = null
    * configure logPrettyResponse = true
    * configure ssl = true
    * configure readTimeout = 1000
    * configure report = {showLog: true, showAllSteps: false}
    # enable X509 certificate authentication with PKCS12 file 'certstore.pfx' and password 'certpassword'
    * configure ssl = { keyStore: 'classpath:certstore.pfx', keyStorePassword: 'certPassword', keyStoreType: 'pkcs12'};
    # trust all server certificates, in the feature file
    * configure ssl = { trustAll: true };

  @RegressionTest
  Scenario:
    Given  path resource
    When method get
    Then status 200
    * def getResult = response
    * match getResult.[0].name contains "AutomationExecution"
    * print getResult