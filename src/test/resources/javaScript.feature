Feature: Practice JavaScript Api

  Background:
  #* def getJavaScript = callonce read('javaScript_1.js')

    * configure proxy = {uri: 'http://my.proxy.host:8080',username: 'MONDAY12,password: 'monf893j3o2'}
    * configure proxy = 'http://my.proxy.host:8080'
    * configure logPrettyResponse = true
    * configure ssl = true
    * configure readTimeout = 3000

  Scenario: JavaScript Demo
    
    Given cookies {cookie:'someValue', moreCoookie:'extraValue', extraCookie: ['avs',228] }
    * params {username: 'lesg', password: 'lkinjd', forgetPassword: 'myname'}
    * headers {Authorization: 'someToken', authoID: '123', extraToken: ['abc', 123] }
    * def user = {name: 'user'}
    * def credentials = {username:'#(user.name)',password:'secret',projects: ['one','two']}
    * form fields credentials
    * match credentials.username = contains{'#(user.name)'}
    * match credentials.project[0] = contains{'one'}
    * print response.credentials.project[1]


    #Test JAVASCRIPT
    * def someJavaScriptFile =
    """
      function myfunction(p1, p2){
        p1 = 1;
        p2 = 2;
        return p1 * p2;
        }
    """

    * print getJavaScript