Feature: Practice post Api

  Scenario: JavaScript Function

#    * def javaScript = function(a, b){ return a + b}
#    * print $.javaScript

    * def dateStringToLong =
  """
  function(s) {
    var SimpleDateFormat = Java.type('java.text.SimpleDateFormat');
    var sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    return sdf.parse(s).time; // '.getTime()' would also have worked instead of '.time'
  }
  """
    * assert dateStringToLong("2016-12-24T03:39:21.081+0000") == 1482550761081