Feature: Authentication

  Background:
    * url 'http://192.168.56.101:3001'
    #https://github.com/auth0-blog/nodejs-jwt-authentication-sample

  @smokeTest
  @sanityTest
  Scenario: Create a user
    Given path 'user'
    When form field username = 'firstuser'
    Then form field password = '1234'
    When method post
    Then status 200

#    Scenario: login and access quote
#    Given path 'session/create'
#    When form field username = 'firstuser'
#    Then form field password = '1234'
#    When method post
#    Then status 201
#    * def accessToken = response.access_token
#
#      Given path 'GET /api/protected/random-quote'
#      And header Authorization = 'Bearer' + accessToken
#      When method GET
#      Then status 200
