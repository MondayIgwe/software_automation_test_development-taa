Feature: Practice post Api
  Background: 
    * url 'http://localhost:3000'
    * header Accept = 'application/json'
    * header ContentType = 'application/json'
    * def resource = '/profile'
    * def createProfile = read('classpath:request.json')


  Scenario:
    Given  path resource
    And request createProfile
    When method post
    Then status 201
    * match createProfile.name[0] == {"name": "AutomationExecution"}
    * print createProfile